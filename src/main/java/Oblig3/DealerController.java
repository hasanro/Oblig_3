package Oblig3;

import Oblig3.DeckOfCards.DeckOfCards;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

/**
 * This class is a controller for dealer.fxml
 */
public class DealerController{

    @FXML
    public Button spadesFlush;
    @FXML
    public Button checkFlush;
    @FXML
    private Button dealHand;
    @FXML
    public Label hand;
    @FXML
    private Label sumOfFaces;
    @FXML
    private Label cardsOfHearts;
    @FXML
    private Label flush;
    @FXML
    private Label queenOfSpades;
    @FXML
    private Button checkHand;

    DeckOfCards deck;

    @FXML
    public void dealHand(ActionEvent actionEvent) throws IOException{
        deck = new DeckOfCards();
        hand.setText(deck.getHandFaceAndSuit(deck.getHand()).toString());
    }

    @FXML
    public void generate(ActionEvent actionEvent) throws IOException{
        if (hand.getText().trim().equalsIgnoreCase(deck.getHandFaceAndSuit(deck.straightFlush()).toString())){
            sumOfFaces.setText(Integer.toString(deck.sumOfFace(deck.straightFlush())));
            cardsOfHearts.setText("");
            flush.setText(deck.flush(deck.straightFlush()) ? "Yes" : "NO");
            queenOfSpades.setText(deck.queenOfSpades(deck.straightFlush()) ? "No" : "Yes");
        } else{
            sumOfFaces.setText(Integer.toString(deck.sumOfFace(deck.getHand())));
            cardsOfHearts.setText(deck.Hearts().toString());
            flush.setText(deck.flush(deck.getHand()) ? "Yes" : "NO");
            queenOfSpades.setText(deck.queenOfSpades(deck.getHand()) ? "No" : "Yes");
        }
    }

    @FXML
    public void dealFlush(ActionEvent actionEvent) throws IOException{
        hand.setText(deck.getHandFaceAndSuit(deck.straightFlush()).toString());
    }

}
