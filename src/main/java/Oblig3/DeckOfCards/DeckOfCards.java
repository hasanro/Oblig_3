package Oblig3.DeckOfCards;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * This class represents a deck of cards
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> playingCards;
    private ArrayList<PlayingCard> straightFlush;
    /**
     * This is the constructor of the class and creates an instance of PlayingCards,
     * fills it with cards, shuffles it and creates a hand of cards.
     */
    public DeckOfCards() {
        this.playingCards = new ArrayList<>();
        this.straightFlush = new ArrayList<>();
        addFlush();
        createDeck();
        shuffleCards();
        getHand();
    }

    /**
     * returns deck
     * @return returns deck
     */
    public ArrayList<PlayingCard> getPlayingCards() {
        return playingCards;
    }


    /**
     * This method fills the deck with cards, if the cards do not already exist
     */
    public void createDeck() {
        List<Face> faces = new ArrayList<>(Arrays.asList(Face.values()));
        List<Suit> suits = new ArrayList<>(Arrays.asList(Suit.values()));
        for (int i = 0; i < faces.size(); i++) {
            for (int j = 0; j < suits.size(); j++) {
                PlayingCard toAdd = new PlayingCard(suits.get(j), faces.get(i));
                if (!(playingCards.contains(toAdd))) {
                    playingCards.add(toAdd);
                }
            }
        }
    }

    /**
     * This method shuffles the deck of cards
     */
    public void shuffleCards() {
        Collections.shuffle(playingCards);
    }

    /**
     * This method generates a hand from the shuffled Deck of cards
     * @return returns a hand of cards
     */
    public ArrayList<PlayingCard> getHand() {
        ArrayList<PlayingCard> hand = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            hand.add(playingCards.get(i));
        }
        return hand;
    }

    /**
     *The method makes a list from the Suit and Face in string form
     * @return returns hand list string
     */
    public ArrayList<PlayingCard> getHandFaceAndSuit(ArrayList<PlayingCard> cards){
        return new ArrayList<>(cards);
    }

    /**
     * This method finds out if queen of spades is a part of the generated hand
     * @return returns true if queen of spades is on hand
     */
    public boolean queenOfSpades(ArrayList<PlayingCard> cards) {
        boolean queenS = false;
        PlayingCard qS = new PlayingCard(Suit.SPADES, Face.QUEEN);
        if (cards.stream().noneMatch(p -> p.equals(qS))){
            queenS = true;
        }
        return queenS;
    }

    /**
     * This method finds if the hand is a flush
     * @return returns true if hand is a flush
     */
    public boolean flush(ArrayList<PlayingCard> cards){
        boolean flush = false;
        if (cards.stream().allMatch(p -> p.getSuit().name().equalsIgnoreCase(Suit.HEARTS.name())) ||
                cards.stream().allMatch(p -> p.getSuit().name().equalsIgnoreCase(Suit.CLUBS.name())) ||
                cards.stream().allMatch(p -> p.getSuit().name().equalsIgnoreCase(Suit.SPADES.name())) ||
                cards.stream().allMatch(p -> p.getSuit().name().equalsIgnoreCase(Suit.DIAMONDS.name()))){
            flush = true;
        }
        return flush;
    }

    /**
     * The method sums the face of the cards on hand
     * @return returns sum of face of hand
     */
    public int sumOfFace(ArrayList<PlayingCard> cards){
        int handSum = 0;
        for (int i = 0; i< cards.size(); i++){
            handSum += cards.get(i).getFace().getFACENUMBER();
        }
        return handSum;
    }

    /**
     * This method finds all heart suit cards and puts them in an arraylist
     * @return returns arraylist with heart cards
     */
    public ArrayList<PlayingCard> Hearts(){
        return getHand().stream()
                .filter(p -> p.getSuit().getSuitChar().equalsIgnoreCase("H "))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public void addFlush(){
        straightFlush.add(new PlayingCard(Suit.SPADES, Face.TEN));
        straightFlush.add(new PlayingCard(Suit.SPADES, Face.JACK));
        straightFlush.add(new PlayingCard(Suit.SPADES, Face.QUEEN));
        straightFlush.add(new PlayingCard(Suit.SPADES, Face.KING));
        straightFlush.add(new PlayingCard(Suit.SPADES, Face.Ace));
    }

    public ArrayList<PlayingCard> straightFlush(){
        return straightFlush;
    }

    /**
     * This method takes in an integer and returns a hand with the size of the integar
     * @param n integer
     * @return a collection of playing cards
     */
    public Collection<PlayingCard> dealHand(int n){
        ArrayList<PlayingCard> dealHand = new ArrayList<>();
        for (int i = 0; i < n; i++){
            dealHand.add(playingCards.get(i));
        }
        Collections.shuffle(dealHand);
        return dealHand;
    }
}