package Oblig3.DeckOfCards;

public enum Suit {
    HEARTS("H "),
    SPADES("S "),
    CLUBS("C "),
    DIAMONDS("D ");

    private final String suitChar;
    Suit(String suitChar) {
        this.suitChar = suitChar;
    }

    public String getSuitChar() {
        return suitChar;
    }
}
