package Oblig3.DeckOfCards;

import java.util.Objects;

/**
 * This class represents a Card
 */
public class PlayingCard {
    private Suit suit;
    private Face face;

    /**
     * Constructor of the class
     * @param suit Suit of the card
     * @param face Face of the card
     */
    public PlayingCard(Suit suit, Face face) {
        this.suit = suit;
        this.face = face;
    }

    public Suit getSuit() {
        return suit;
    }

    public Face getFace() {
        return face;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public void setFace(Face face) {
        this.face = face;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayingCard that = (PlayingCard) o;
        return suit == that.suit && face == that.face;
    }


    @Override
    public String toString() {
        return " " + face.getFACE() + suit.getSuitChar() + " ";
    }
}
